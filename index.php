<?php 
/*
* Mnimal controller to handle routes
* SEE https://github.com/noodlehaus/dispatch/tree/8.0.4
*/

require_once 'vendor/dispatch.php';
require_once 'vendor/autoload.php';

config(parse_ini_file('config.ini'));
$twig_setup = [];

if(!config('debug')) {
  // Turn on twig caching when debug is off.
  $twig_setup = ['cache' => 'cache'];
}

// Load twig for templates
$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader, $twig_setup);
$twig->addGlobal('url', config('wr'));
$twig->addGlobal('debug', config('debug'));


//Routes and actions
map(['', 'home'], function () use ($twig) {
	echo $twig->render('home.html');
});

map('lorempage', function () use ($twig) {
    echo $twig->render('lorem.html');
});

// Example POST action
map('POST', '/contact', function () {
  $data = $_POST;
  // TODO: Send mail here
});

// Error codes
map(404, function ($code) use ($twig) {
 	echo $twig->render('404.html');
});


//Handle the request
dispatch();